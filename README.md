## 我的补番列表

> Created by [miku酱](https://space.bilibili.com/37082690) on March , 2019

> Modified on April 08, 2019

----

欢迎在 issues 和我一起讨论有关话题~

（PS：该列表按照追番时间排序。）

- [ ] 钢之炼金术师 FULLMETAL ALCHEMIST（[鋼の錬金術師](https://myanimelist.net/anime/121/Fullmetal_Alchemist)）
- [ ] 钢之炼金术师 FULLMETAL ALCHEMIST（Movie）
- [x] 欢迎来到实力至上主义的教室（[ようこそ実力至上主义 の教室へ](https://myanimelist.net/anime/35507/Youkoso_Jitsuryoku_Shijou_Shugi_no_Kyoushitsu_e)）
- [x] 干物妹！小埋（第一季）（[干物妹！うまるちゃん](https://myanimelist.net/anime/28825/Himouto_Umaru-chan)）
- [x] 干物妹！小埋R（第二季）（[干物妹！うまるちゃんR](https://myanimelist.net/anime/35376/Himouto_Umaru-chan_R)）
- [x] 干物妹！小埋R（OVA）（[干物妹！うまるちゃん OVA](https://myanimelist.net/anime/30991/Himouto_Umaru-chan_OVA)）
- [x] 小林家的龙女仆（[小林さんちのメイドラゴン](https://myanimelist.net/anime/33206/Kobayashi-san_Chi_no_Maid_Dragon)）
- [x] 小林家的龙女仆（OVA）（[小林さんちのメイドラゴン](https://myanimelist.net/anime/35145/Kobayashi-san_Chi_no_Maid_Dragon_Specials)）
- [ ] 一起一起这里那里（[あっちこっち](https://myanimelist.net/anime/12291/Acchi_Kocchi_TV)）
- [ ] 我的妹妹不可能那么可爱（第一季）（[俺の妹がこんなに可愛いわけがない](https://myanimelist.net/anime/8769/Ore_no_Imouto_ga_Konnani_Kawaii_Wake_ga_Nai)）
- [ ] 我的妹妹不可能那么可爱（第二季）
- [ ] 妖精森林的小不点（[ハクメイとミコチ](https://myanimelist.net/anime/36094/Hakumei_to_Mikochi)）
- [ ] 如果有妹妹就好了。（[妹さえいればいい。](https://myanimelist.net/anime/35413/Imouto_sae_Ireba_Ii)）
- [ ] 如果有妹妹就好了。（Movie）
- [ ] 星之梦（[planetarian～星の人～](https://myanimelist.net/anime/33190/Planetarian__Hoshi_no_Hito)）
- [ ] CLANNAD（第一季）（[CLANNAD](https://myanimelist.net/anime/2167/Clannad)）
- [ ] CLANNAD（第二季）
- [ ] CLANNAD（OVA）
- [x] 四月是你的谎言（[四月は君の嘘](https://myanimelist.net/manga/37707/Shigatsu_wa_Kimi_no_Uso)）
- [x] 在下坂本，有何贵干？（[坂本ですが？](https://myanimelist.net/anime/32542/Sakamoto_Desu_ga)）
- [x] 猫咪日常（[にゃんこデイズ](https://myanimelist.net/anime/34148/Nyanko_Days)）
- [ ] Re：从零开始的异世界生活（[Re：ゼロから始める異世界生活](https://myanimelist.net/anime/31240/Re_Zero_kara_Hajimeru_Isekai_Seikatsu)）
- [ ] 当女孩遇到熊（[くまみこ](https://myanimelist.net/anime/31804/Kuma_Miko)）
- [ ] 当女孩遇到熊（SP）
- [ ] 路人女主的养成方法（[冴えない彼女〈ヒロイン〉の育てかた](https://myanimelist.net/anime/23277/Saenai_Heroine_no_Sodatekata)）
- [ ] 青春猪头少年不会梦到兔女郎学姐（[青春ブタ野郎はバニーガール先輩の夢を見ない](https://myanimelist.net/anime/37450/Seishun_Buta_Yarou_wa_Bunny_Girl_Senpai_no_Yume_wo_Minai)）
- [x] 夏目友人帐（第一季）（[夏目友人帳](https://myanimelist.net/anime/4081/Natsume_Yuujinchou)）
- [ ] 夏目友人帐（第二季）
- [ ] 夏目友人帐（第三季）
- [ ] 夏目友人帐（第四季）
- [ ] 夏目友人帐（第五季）
- [ ] 夏目友人帐（第六季）
- [ ] 夏目友人帐（OVA）
- [ ] 樱花庄的宠物女孩（[さくら荘のペットな彼女](https://myanimelist.net/anime/13759/Sakurasou_no_Pet_na_Kanojo)）
- [ ] 魔法禁书目录（第一季）（[とある魔術の禁書目録](https://myanimelist.net/anime/4654/Toaru_Majutsu_no_Index)）
- [ ] 魔法禁书目录（第二季）
- [ ] 魔法禁书目录（第三季）
- [ ] 魔法禁书目录（SP）
- [ ] 魔法科高校的劣等生（[魔法科高校の劣等生](https://myanimelist.net/anime/20785/Mahouka_Koukou_no_Rettousei)）
- [ ] 工作细胞（[はたらく細胞](https://myanimelist.net/anime/37141/Hataraku_Saibou)）
- [ ] 笨女孩（[アホガール](https://myanimelist.net/anime/34881/Aho_Girl)）
- [ ] 约会大作战（第一季）（[デート・ア・ライブ](https://myanimelist.net/anime/15583/Date_A_Live)）
- [ ] 约会大作战（第二季）
- [ ] 约会大作战（第三季）
- [ ] 约会大作战（Movie）
- [ ] 某科学的超电磁炮（第一季）（[とある科学の超電磁砲](https://myanimelist.net/anime/6213/Toaru_Kagaku_no_Railgun)）
- [ ] 某科学的超电磁炮（第二季）
- [ ] 某科学的超电磁炮（OVA）
- [ ] Angel Beats!（[Angel Beats!（エンジェルビーツ）](https://myanimelist.net/anime/6547/Angel_Beats)）
- [ ] DARLING in the FRANXX（[ダーリン・イン・ザ・フランキス](https://myanimelist.net/anime/35849/Darling_in_the_FranXX)）
- [ ] 五等分的花嫁（[五等分の花嫁](https://myanimelist.net/manga/103851/5-toubun_no_Hanayome)）
- [x] 辉夜大小姐想让我告白~天才们的恋爱头脑战~（[かぐや様は告らせたい～天才たちの恋愛頭脳戦～](https://myanimelist.net/anime/37999/Kaguya-sama_wa_Kokurasetai__Tensai-tachi_no_Renai_Zunousen)）
- [ ] 我们无法一起学习（[ぼくたちは勉強ができない](https://myanimelist.net/manga/103890/Bokutachi_wa_Benkyou_ga_Dekinai)）
- [ ] 关于我转生变成史莱姆这档事（[転生したらスライムだった件](https://myanimelist.net/anime/37430/Tensei_shitara_Slime_Datta_Ken)）
- [ ] 川柳少女（[川柳少女](https://myanimelist.net/anime/38787/Senryuu_Shoujo)）
- [ ] 一个人的〇〇小日子（[ひとりぼっちの○○生活](https://myanimelist.net/manga/89467/Hitoribocchi_no_%E2%97%8B%E2%97%8B_Seikatsu)）
- [ ] 水果篮子(2019)（[フルーツバスケット](https://myanimelist.net/anime/38680/Fruits_Basket_2019)）
- [ ] 天使降临到我身边（[私に天使が舞い降りた！](https://myanimelist.net/anime/37993/Watashi_ni_Tenshi_ga_Maiorita)）
- To be continued.... 未完待续~
